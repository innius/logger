package inniuslog

import (
	"fmt"
	"github.com/sirupsen/logrus"
)

type LogLevel int

const (
	LogLevelDebug LogLevel = iota + 1
	LogLevelInfo
	LogLevelWarn
	LogLevelError
)

type Logger interface {
	// Log can be called concurrently from multiple goroutines so make sure
	// your implementation is goroutine safe.
	Log(level LogLevel, msg fmt.Stringer, fields Fields)
}

func (ll LogLevel) String() string {
	switch ll {
	case LogLevelDebug:
		return "DEBUG"
	case LogLevelInfo:
		return "INFO"
	case LogLevelWarn:
		return "WARN"
	case LogLevelError:
		return "ERROR"
	default:
		return fmt.Sprintf("UNKNOWNLOGLEVEL<%d>", ll)
	}
}

func NewDefaultLogger(minLogLevel LogLevel) Logger {
	return &defaultLogger{Logger: logrus.New(), minLevel: minLogLevel}
}

type defaultLogger struct {
	*logrus.Logger
	minLevel LogLevel
}

type Fields map[string]interface{}

func (l *defaultLogger) Log(ll LogLevel, msg fmt.Stringer, fields Fields) {
	if ll < l.minLevel {
		return
	}
	switch ll {
	case LogLevelDebug:
		l.Debug(msg)
	case LogLevelInfo:
		l.Info(msg)
	case LogLevelWarn:
		l.Warn(msg)
	case LogLevelError:
		l.Error(msg)
	}
}

var NullLogger = nullLogger{}

type nullLogger struct{}

func (nl nullLogger) Log(ll LogLevel, msg fmt.Stringer) {}
