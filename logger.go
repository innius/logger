package logger

import (
	"bitbucket.org/innius/logger/inniuslog"
	"bitbucket.org/to-increase/go-api-context"
	"fmt"
)

var Logger = inniuslog.NewDefaultLogger(inniuslog.LogLevelInfo)

func Debugf(format string, args ...interface{}) {
	Logger.Log(inniuslog.LogLevelDebug, printfArgs{format, args}, nil)
}

func Debug(args ...interface{}) {
	Logger.Log(inniuslog.LogLevelDebug, printArgs(args), nil)
}

func DebugContext(c apicontext.ApiContext, args ...interface{}) {
	Logger.Log(inniuslog.LogLevelDebug, printArgs(args), contextFields(c))
}

func DebugfContext(c apicontext.ApiContext, format string, args ...interface{}) {
	Logger.Log(inniuslog.LogLevelDebug, printfArgs{format, args}, contextFields(c))
}

func Infof(format string, args ...interface{}) {
	Logger.Log(inniuslog.LogLevelInfo, printfArgs{format, args}, nil)
}

func Info(args ...interface{}) {
	Logger.Log(inniuslog.LogLevelInfo, printArgs(args), nil)
}

func Errorf(format string, args ...interface{}) {
	Logger.Log(inniuslog.LogLevelError, printfArgs{format, args}, nil)
}

func Error(args ...interface{}) {
	Logger.Log(inniuslog.LogLevelError, printArgs(args), nil)
}

type printfArgs struct {
	format string
	args   []interface{}
}

func (p printfArgs) String() string {
	return fmt.Sprintf(p.format, p.args...)
}

type printArgs []interface{}

func (p printArgs) String() string {
	return fmt.Sprint([]interface{}(p)...)
}

func contextFields(c apicontext.ApiContext) inniuslog.Fields {
	return map[string]interface{}{
		"domain": c.Domain(),
		"userid": c.UserID(),
	}
}
