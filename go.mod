module bitbucket.org/innius/logger

go 1.12

require (
	bitbucket.org/to-increase/go-api-context v1.0.0
	github.com/sirupsen/logrus v1.4.0
)
