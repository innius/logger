package logger

import (
	"bitbucket.org/innius/logger/inniuslog"
	"bitbucket.org/to-increase/go-api-context"
	"context"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type testLogger struct {
	level  inniuslog.LogLevel
	fields inniuslog.Fields
	msg    fmt.Stringer
}

func (nl *testLogger) Log(ll inniuslog.LogLevel, msg fmt.Stringer, fields inniuslog.Fields) {
	nl.level = ll
	nl.msg = msg
	nl.fields = fields
}

const domainKey = "domain"

func TestLogger(t *testing.T) {
	Convey("Logger Tests", t, func() {
		tl := &testLogger{}
		Logger = tl
		message := "foo"
		format := "%s"
		ctx := apicontext.WithDomain(context.Background(), "foo.bar")
		Convey("debug", func() {
			Debug(message)
			So(tl.level, ShouldEqual, inniuslog.LogLevelDebug)
			So(tl.msg.String(), ShouldEqual, message)
		})
		Convey("debug with context", func() {
			DebugContext(ctx, message)
			So(tl.level, ShouldEqual, inniuslog.LogLevelDebug)
			So(tl.msg.String(), ShouldEqual, message)
			So(tl.fields, ShouldContainKey, domainKey)
			So(tl.fields[domainKey], ShouldEqual, ctx.Domain())
		})
		Convey("debugf with context", func() {
			DebugfContext(ctx, format, message)
			So(tl.level, ShouldEqual, inniuslog.LogLevelDebug)
			So(tl.msg.String(), ShouldEqual, message)
			So(tl.fields, ShouldContainKey, domainKey)
			So(tl.fields[domainKey], ShouldEqual, ctx.Domain())
		})
	})
}
